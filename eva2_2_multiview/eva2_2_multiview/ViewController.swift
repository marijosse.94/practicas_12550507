//
//  ViewController.swift
//  eva2_2_multiview
//
//  Created by Majo Molina on 19/12/16.
//  Copyright © 2016 mjmm. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var rosaViewController: RosaViewController!
    var verdeViewController: VerdeViewController!
    
    @IBAction func cambiarVista(sender: UIBarButtonItem) {
        if  rosaViewController?.view.superview == nil{//NO ES VISIBLE AL USUARIO
            if rosaViewController == nil {//EL CONTROLADOR NO EXISTE
                rosaViewController = storyboard?.instantiateViewController(withIdentifier: "Rosa") as! RosaViewController
            }
        }else
            if verdeViewController?.view.superview == nil{//NO ES VISIBLE AL USUARIO
                if verdeViewController == nil {//EL CONTROLADOR NO EXISTE
                    verdeViewController = storyboard?.instantiateViewController(withIdentifier: "VERDE") as! VerdeViewController
                }
        }
        
        
        //INTERCAMBIAMOS VISTAS
        
        UIView.beginAnimations("Mi animacion", context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationCurve(.easeInOut)
        
        if rosaViewController != nil && rosaViewController!.view.superview != nil {
            
            UIView.setAnimationTransition(.flipFromRight, for: view, cache: true)
            verdeViewController.view.frame = view.frame
            //FALTAAGREGAR LAS VISTAS
            cambiarVistas(desde: rosaViewController, hasta: verdeViewController)
        }else{
            UIView.setAnimationTransition(.flipFromRight, for: view, cache: true)
            rosaViewController.view.frame = view.frame
            //FALTAAGREGAR LAS VISTAS
            cambiarVistas(desde: verdeViewController, hasta: rosaViewController)
        }
        
        UIView.commitAnimations()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //MOSTRAR UNA DE LOS DOS ESCENAS QUE TENEMOS (AZUL O VERDE)
        rosaViewController = storyboard?.instantiateViewController(withIdentifier: "ROSA") as! RosaViewController
        rosaViewController.view.frame = view .frame
        //FALTAAGREGAR LAS VISTAS
        cambiarVistas(desde: nil, hasta: rosaViewController)
        
    }
    
    private func cambiarVistas(desde vistaActual: UIViewController?, hasta vistaNueva: UIViewController?){
        if vistaActual != nil {
            vistaActual!.willMove(toParentViewController: nil)
            vistaActual!.view.removeFromSuperview()
            vistaActual!.removeFromParentViewController()
        }
        
        if vistaNueva != nil {
            addChildViewController(vistaNueva!)
            view.insertSubview(vistaNueva!.view, at: 0)
            vistaNueva!.didMove(toParentViewController: self)
        }
        
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        if rosaViewController != nil && rosaViewController.view.superview == nil {
            rosaViewController = nil
        }
        
        if verdeViewController != nil && verdeViewController.view.superview == nil {
            verdeViewController = nil
        }
        
}

}
