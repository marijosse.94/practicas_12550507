//
//  NaranjaViewController.swift
//  EVA2_3_MULTIVIEW2
//
//  Created by Majo Molina on 14/10/16.
//  Copyright © 2016 Majo. All rights reserved.
//
import UIKit

class NaranjaViewController: UIViewController {
    
    var colores = ["morado","azul","Negro","Verde","Cafe","Rosa",
        "Rojo","Amarillo","Naranja","Plata","Dorado","Violeta",
        "Gris",]
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "celda"
        let celda = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for:  indexPath)
        //ponemos los datos
        
        celda.textLabel?.text = colores[indexPath.row]
        return celda
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colores.count
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
