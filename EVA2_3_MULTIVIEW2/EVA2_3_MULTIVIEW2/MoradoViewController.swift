//
//  MoradoViewController.swift
//  EVA2_3_MULTIVIEW2
//
//  Created by Majo Molina on 14/10/16.
//  Copyright © 2016 Majo. All rights reserved.
//

import UIKit

class MoradoViewController: UIViewController {

    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func onClick(_ sender: UIDatePicker) {
        
        let date = datePicker.date
        let alerta = UIAlertController( title: "Fecha seleccionada",message: "La Fecha Fs \(date)",
                                        preferredStyle: .alert)
        
        let accion = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let date = Date()
        datePicker.setDate(date, animated: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
