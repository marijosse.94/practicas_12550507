//
//  ViewController.swift
//  EVA2_1_TABLAS_Y_PROTOCOLOS
//
//  Created by Majo Molina on 16/12/16.
//  Copyright © 2016 mjmm. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var colores = ["Morado","Azul","Negro","Cafe","Gris","Verde","Blanco","Plata","Dorado","Rosa"]
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let colores = self.colores[indexPath.row]
        let notif = UIAlertController(title: "Colores ", message: "Tu color: \(colores)", preferredStyle: .alert)
        let onClick = UIAlertAction(title: "ok", style: .destructive, handler: nil)
        notif.addAction(onClick)
        present(notif, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colores.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath)
        cell.textLabel?.text = colores[indexPath.row]
        return cell
    }
    
  
}

