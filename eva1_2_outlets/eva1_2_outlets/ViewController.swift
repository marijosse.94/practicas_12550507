//
//  ViewController.swift
//  eva1_2_outlets
//
//  Created by Majo Molina on 19/12/16.
//  Copyright © 2016 mjmm. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblMsn: UILabel!
    @IBAction func onClick(_ sender: Any) {
        lblMsn.text = "NEW TEXT"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

